extends StaticBody2D

var radius = 48
var cancelled = false

signal cancelled

# Called when the node enters the scene tree for the first time.
func init():
	# Set radius for sprite and collision shape
	set_size()
	
	# Set initial position
	change_position()


func set_size():
	var img_size = $Sprite.get_texture().get_size() #image size
	var scale = Vector2(radius*2/img_size.x, radius*2/img_size.y)
	$Sprite.set_scale(scale)
	$CollisionShape2D.shape.radius = radius
	$Area2D/CollisionShape2D.shape.radius = radius


func change_position():
	var screen_size = get_viewport_rect().size
	var min_x = radius
	var max_x = screen_size.x - radius
	var min_y = screen_size.y * 0.3
	var max_y = screen_size.y * 0.6
	
	set_position(Vector2(randi() % int(max_x - min_x) + min_x, randi() % int(max_y - min_y) + min_y))
	
	begin_in()


func _on_OutSound_finished():
	change_position()


func begin_out():
	$OutSound.play()
	$Tween.interpolate_property($Sprite.get_material(), "shader_param/whiteness", 0.0, 1.0, 1.0, Tween.TRANS_QUART, Tween.EASE_OUT)
	$Tween.start()
	$CollisionShape2D.set_deferred("disabled", true)
	$Area2D/CollisionShape2D.set_deferred("disabled", false)


func begin_in():
	$InSound.play()
	$Tween.interpolate_property($Sprite.get_material(), "shader_param/whiteness", 1.0, 0.0, 1.0, Tween.TRANS_QUART, Tween.EASE_OUT)
	$Tween.start()
	cancelled = false


func _on_InSound_finished():
	if cancelled:
		emit_signal("cancelled")
		begin_out()
	else:
		$Area2D/CollisionShape2D.set_deferred("disabled", true)
		$CollisionShape2D.set_deferred("disabled", false)


func _on_Area2D_body_entered(_body):
	cancelled = true
