extends Area2D

signal got_ball


func _on_TopBoundary_body_entered(body):
	emit_signal("got_ball", false, body)
