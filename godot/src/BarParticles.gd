extends CPUParticles2D

signal done

func _process(_delta):
	if not emitting:
		emit_signal("done")
		queue_free()
