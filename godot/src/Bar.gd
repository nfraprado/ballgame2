extends StaticBody2D

var thickness = Globals.bar_thickness
var min_length = 20

var min_sound_pitch = 0.9
var max_sound_pitch = 1.1


func _ready():
	$SpritePos/NinePatchRect.set_position(Vector2(0, -thickness/2))


func get_length(pos1, pos2):
	return (pos1 - pos2).length()


func get_angle(pos1, pos2):
	return atan2(pos2.y-pos1.y, pos2.x-pos1.x)


func update_bar(pos1, pos2, play_sound=true):
	var length = get_length(pos1, pos2)
	
	if length < min_length:
		$CollisionShape2D.disabled = true
		$SpritePos/NinePatchRect.visible = false
		return
	
	if play_sound:
		var sound_pitch = randf() * (max_sound_pitch - min_sound_pitch) + min_sound_pitch
		$BarDoneSound.pitch_scale = sound_pitch
		$BarDoneSound.play()
	
	$CollisionShape2D.disabled = false
	$SpritePos/NinePatchRect.visible = true
	
	# Set dimensions for sprite and collision shape
	$SpritePos/NinePatchRect.margin_right = (pos2 - pos1).length()
	$SpritePos/NinePatchRect.margin_bottom = thickness/2
	$CollisionShape2D.shape.set_extents(Vector2(length/2, thickness/2))
	
	# Set rotation for sprite and collision shape
	var angle = get_angle(pos1, pos2)
	$SpritePos.set_rotation(angle)
	$CollisionShape2D.set_rotation(angle)
	
	# Set position for collision shape, relative to center of Bar
	$CollisionShape2D.set_position((pos2-pos1)/2)


func _on_GhostBar_bar_drawn(pos1, pos2):
	set_position(pos1)
	update_bar(pos1, pos2)
