extends Node2D

export(int, "ButtonPlay", "ButtonSettings", "ButtonCredits", "ButtonMenu") var type
export (int) var radius
signal pressed

var play_button_pressed = preload("res://assets/img/ball_button_play_pressed.png")
var settings_button_pressed = preload("res://assets/img/ball_button_settings_pressed.png")
var credits_button_pressed = preload("res://assets/img/ball_button_credits_pressed.png")
var menu_button_pressed = preload("res://assets/img/ball_button_menu_pressed.png")

# Called when the node enters the scene tree for the first time.
func _ready():
	# Translate BallButton type to Ball type
	$Ball.type = type + 3
	
	if type == 0:
		$Ball/Button.set_pressed_texture(play_button_pressed)
	elif type == 1:
		$Ball/Button.set_pressed_texture(settings_button_pressed)
	elif type == 2:
		$Ball/Button.set_pressed_texture(credits_button_pressed)
	elif type == 3:
		$Ball/Button.set_pressed_texture(menu_button_pressed)
	
	#$Ball/Button.get_pressed_texture().set_size(Vector2(radius * 2, radius * 2))
	$Ball/Button.set_position(Vector2(-radius, -radius))
	$Ball/Button.set_size(Vector2(radius * 2, radius * 2))
	
	$Ball.init_fixed(radius)


func _on_Button_pressed():
	emit_signal("pressed")


func _on_Button_button_down():
	$Ball/Button/ButtonPressSound.play()
