extends Node2D

export(int, "Left", "Center", "Right") var type

signal got_ball(type)

var left_bucket_sprite = preload("res://assets/img/bucket_left.png")
var center_bucket_sprite = preload("res://assets/img/bucket_center.png")
var right_bucket_sprite = preload("res://assets/img/bucket_right.png")

var shine_done = true

func _on_Bottom_body_entered(body):
	if body.type == type:
		$RightBallSound.play()
		shine(true, true)
		emit_signal("got_ball", true, body)
	else:
		$WrongBallSound.play()
		shine(true, false)
		emit_signal("got_ball", false, body)


func shine(first, white):
	if white:
		if first:
			$RightShine.interpolate_property($Sprite.get_material(), "shader_param/whiteness", 0.0, 0.5, 0.3, Tween.TRANS_QUART, Tween.EASE_OUT)
			$RightShine.start()
			shine_done = false
		elif shine_done == false:
			$RightShine.interpolate_property($Sprite.get_material(), "shader_param/whiteness", 0.5, 0.0, 0.3, Tween.TRANS_QUART, Tween.EASE_OUT)
			$RightShine.start()
			shine_done = true
		else:
			pass
	else:
		if first:
			$WrongShine.interpolate_property($Sprite.get_material(), "shader_param/whiteness", 0.0, -0.5, 0.3, Tween.TRANS_QUART, Tween.EASE_OUT)
			$WrongShine.start()
			shine_done = false
		elif shine_done == false:
			$WrongShine.interpolate_property($Sprite.get_material(), "shader_param/whiteness", -0.5, 0.0, 0.3, Tween.TRANS_QUART, Tween.EASE_OUT)
			$WrongShine.start()
			shine_done = true
		else:
			pass


func _ready():
	if type == 0:
		$Sprite.set_texture(left_bucket_sprite)
	if type == 1:
		$Sprite.set_texture(center_bucket_sprite)
	if type == 2:
		$Sprite.set_texture(right_bucket_sprite)


func _on_WrongShine_tween_all_completed():
	shine(false, false)


func _on_RightShine_tween_all_completed():
	shine(false, true)
