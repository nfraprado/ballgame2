extends Node2D


# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	
	var screen_size = get_viewport_rect().size
	var pos_bar_y = screen_size.y / 4 * 3
	
	$Bar.call_deferred("set_position", Vector2(0, pos_bar_y))
	$Bar.call_deferred("update_bar", Vector2(0, pos_bar_y), Vector2(screen_size.x, pos_bar_y), false)


func _on_PlayButton_pressed():
	get_tree().change_scene("res://src/Game.tscn")


func _on_MenuButton_pressed():
	get_tree().change_scene("res://src/Menu.tscn")
