extends Area2D


func _ready():
	set_position(Vector2(Globals.bar_thickness/2, 0))
	$CollisionShape2D.get_shape().set_extents(Vector2(Globals.bar_thickness/2, Globals.bar_thickness/2))
