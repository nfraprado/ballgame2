extends Node2D

var vel_sq = 0.2/3
var vel_tri = 0.2/2
var vel_st = 0.2

var color = Color(0, 0, 0, 1)
var color_dir = Color(0, 0, 0, 1)
var color_step = 0.03
var max_brightness = 0.5

func _ready():
	set_position(get_viewport_rect().size / 2)


# Called when the node enters the scene tree for the first time.
func _process(delta):
	if (color + color_dir * delta).v < max_brightness:
		color += color_dir * delta
	$Square.set_rotation($Square.get_rotation() + vel_sq * delta / (2 * PI))
	$StarNode.set_rotation($StarNode.get_rotation() + vel_st * delta / (2 * PI))
	$TriangleNode.set_rotation($TriangleNode.get_rotation() + vel_tri * delta / (2 * PI))
	$Square.set_modulate(color)


func _on_ColorWalk_timeout():
	color_dir = Color(rand_range(-1, 1) * color_step, rand_range(-1, 1) * color_step, rand_range(-1, 1) * color_step, 1)
