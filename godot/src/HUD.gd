extends CanvasLayer

var score = 0

func update_score(delta_score):
	score += delta_score
	$MarginContainer/ScoreLabel.text = str(score)
