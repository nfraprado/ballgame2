extends RigidBody2D

export(int, "Left", "Center", "Right", "ButtonPlay", "ButtonConfig", "ButtonCredits") var type

var max_radius = 100 # Size of the bucket aperture is 144
var min_radius = 50 # Half of max size
var max_impulse = 300

# Constants for calculating sound volume
var min_volume_db = -30
var max_ball_velocity = 1200

var min_sound_pitch = 0.8
var max_sound_pitch = 1.2

var left_ball_sprite = preload("res://assets/img/ball_left.png")
var center_ball_sprite = preload("res://assets/img/ball_center.png")
var right_ball_sprite = preload("res://assets/img/ball_right.png")

var play_button_sprite = preload("res://assets/img/ball_button_play.png")
var settings_button_sprite = preload("res://assets/img/ball_button_settings.png")
var credits_button_sprite = preload("res://assets/img/ball_button_credits.png")
var menu_button_sprite = preload("res://assets/img/ball_button_menu.png")

var vel = Vector2(0, 0)

func init_rand():
	# Randomly set color
	self.type = randi() % 3
		
	# Set correct sprite
	if type == 0:
		$Sprite.set_texture(left_ball_sprite)
	if type == 1:
		$Sprite.set_texture(center_ball_sprite)
	if type == 2:
		$Sprite.set_texture(right_ball_sprite)
	
	var radius = randi() % (max_radius - min_radius) + min_radius
	# Set radius for sprite and collision shape
	init_size(radius)

	# Set initial position
	randomize_pos(radius)
	
	# Give random initial velocity
	randomize_vel(radius)


func init_fixed(radius):
	if type == 3:
		$Sprite.set_texture(play_button_sprite)
	if type == 4:
		$Sprite.set_texture(settings_button_sprite)
	if type == 5:
		$Sprite.set_texture(credits_button_sprite)
	if type == 6:
		$Sprite.set_texture(menu_button_sprite)
	init_size(radius)
	randomize_vel(radius)


func init_size(radius):
	var img_size = left_ball_sprite.get_size() #image size
	var scale = Vector2(radius*2/img_size.x, radius*2/img_size.y)
	$Sprite.set_scale(scale)
	$CollisionShape2D.shape.radius = radius


func randomize_pos(radius):
	var screen_size = get_viewport_rect().size
	var min_x = radius
	var max_x = screen_size.x - radius
	set_position(Vector2(randi() % int(max_x - min_x) + min_x, -radius))


func randomize_vel(radius):
	var offset = Vector2(randi() % radius * 2 - radius, randi() % radius * 2 - radius)
	var impulse = Vector2(randi() % max_impulse * 2 - max_impulse, randi() % max_impulse)
	apply_impulse(offset, impulse)


func _on_Ball_body_entered(_body):
	var cur_vel = get_linear_velocity()
	var impact = (cur_vel - vel).length()/max_ball_velocity
	var volume_db = 10 * log(impact)
	var sound_pitch = randf() * (max_sound_pitch - min_sound_pitch) + min_sound_pitch
	
	if volume_db > min_volume_db: 
		$BallCollisionSound.volume_db = volume_db
		$BallCollisionSound.pitch_scale = sound_pitch
		$BallCollisionSound.play()


func _physics_process(_delta):
	vel = get_linear_velocity()
