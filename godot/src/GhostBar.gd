extends Node2D


signal bar_drawn(pos1, pos2)

var bar_particles_scene = preload("res://src/BarParticles.tscn")
var bar_particles = []
var particle_separation = 60

var mouse_pos = Vector2(0, 0)
var mouse_pressed

var top_pos = Vector2(0, -Globals.bar_thickness/2)
var bottom_pos = -top_pos

var inside_body = false

func _input(event):
	if event is InputEventMouseMotion:
		if mouse_pressed:
			mouse_pos = to_local(event.position)
			update_collision()
	if event is InputEventMouseButton:
		if event.pressed:
			set_position(event.position)
			mouse_pos = Vector2(0, 0)
			update_collision()
			mouse_pressed = true
			$TopRay.enabled = true
			$BottomRay.enabled = true
			
			$BarDrawSound.play()
		else:
			emit_signal("bar_drawn", position,
						get_parent().to_local(to_global(get_end())))
			$BarDrawSound.stop()
			
			mouse_pressed = false
			$TopRay.enabled = false
			$BottomRay.enabled = false
			remove_extra_particles(len(bar_particles))


func _process(_delta):
	if mouse_pressed:
		place_bar_particles(get_end())


func update_collision():
	var angle = atan2(mouse_pos.y, mouse_pos.x)
	
	$TopRay.set_position(top_pos.rotated(angle))
	$BottomRay.set_position(bottom_pos.rotated(angle))
	$TopRay.set_cast_to(mouse_pos)
	$BottomRay.set_cast_to(mouse_pos)
	
	$Area2D.set_rotation(angle)
	$Area2D.set_position(Vector2(Globals.bar_thickness/2, 0).rotated(angle))


func get_end():
	var angle = atan2(mouse_pos.y, mouse_pos.x)
	var top_collision
	var bottom_collision
	
	if inside_body:
		return Vector2(0, 0)
	
	# It looks ugly but is the only thing that worked. Rewrite with caution.
	if !$TopRay.is_colliding() and !$BottomRay.is_colliding():
		return mouse_pos
	elif $TopRay.is_colliding() and !$BottomRay.is_colliding():
		top_collision = to_local($TopRay.get_collision_point())
		return top_collision - top_pos.rotated(angle)
	elif $BottomRay.is_colliding() and !$TopRay.is_colliding():
		bottom_collision = to_local($BottomRay.get_collision_point())
		return bottom_collision - bottom_pos.rotated(angle)
	else:
		top_collision = to_local($TopRay.get_collision_point())
		bottom_collision = to_local($BottomRay.get_collision_point())
		if top_collision.length() < bottom_collision.length():
			return top_collision - top_pos.rotated(angle)
		else:
			return bottom_collision - bottom_pos.rotated(angle)


func get_particle_places(end_pos):
	var num_particles = floor(end_pos.length()/particle_separation)
	
	var positions = []
	positions.append(Vector2(0, 0))
	positions.append(end_pos)
	
	if num_particles > 1:
		for i in range(1, num_particles):
			positions.append(end_pos * i / num_particles)
	
	return positions


func place_bar_particles(end_pos):
	var positions = get_particle_places(end_pos)
	
	var particle_positions = []
	for particle in bar_particles:
		particle_positions.append(particle.get_position())
	
	if len(positions) > len(particle_positions):
		var missing_positions = []
		for i in range(len(particle_positions), len(positions)):
			missing_positions.append(positions[i])
		add_particles(missing_positions)
	elif len (positions) < len(particle_positions):
		remove_extra_particles(len(particle_positions) - len(positions))
	
	if particle_positions != positions:
		update_particle_positions(positions)


func add_particles(missing_positions):
	for position in missing_positions:
		var particle = bar_particles_scene.instance()
		add_child(particle)
		particle.set_position(position)
		bar_particles.append(particle)
		particle.emitting = true


func remove_extra_particles(num):
	for _i in range(num):
		var particle = bar_particles.pop_back()
		particle.queue_free()


func update_particle_positions(positions):
	for i in range(len(positions)):
		var particle = bar_particles[i]
		var pos = positions[i]
		
		if particle.get_position() != pos:
			particle.set_position(pos)


func _on_Area2D_body_entered(_body):
	inside_body = true


func _on_Area2D_body_exited(_body):
	inside_body = false
