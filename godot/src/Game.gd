extends Node2D

var ball_scene = preload("res://src/Ball.tscn")
var obstacle_scene = preload("res://src/Obstacle.tscn")
var over_menu_scene = preload("res://src/OverMenu.tscn")

var balls = []
var obstacles = []

func _ready():
	randomize()
	new_ball()


func _input(event):
	if event is InputEventKey: # just for debugging
		if event.pressed and event.scancode == KEY_Q: 
			get_tree().quit()


func _on_Bucket_got_ball(correct, body):
	if correct:
		$HUD.update_score(len(balls))
		call_deferred("new_ball")
	else:
		if len(balls) <= 1: # Shouldn't ever be less than 1
			game_over()
	
	remove_ball(body)


func _on_Timer_timeout():
	new_ball()


func _on_TimerObstacle_timeout():
	change_obstacle()


func new_ball():
	var ball = ball_scene.instance()
	
	add_child(ball)
	ball.init_rand()
	balls.append(ball)


func remove_ball(body):
	balls.erase(body)
	body.queue_free()


func new_obstacle():
	var obstacle = obstacle_scene.instance()
	obstacles.append(obstacle)
	
	add_child(obstacle)
	obstacle.connect("cancelled", self, "_obstacle_cancelled")
	obstacle.init()

func remove_obstacle():
	var obstacle = obstacles.pop_back()
	if obstacle:
		remove_child(obstacle)

func change_obstacle():
	$Obstacle.begin_out()

func _obstacle_cancelled():
	$TimerObstacle.start()

func game_over():
	$TimerBall.stop()
	$TimerObstacle.stop()
	$TimerInitObstacle.stop()
	remove_child($Bar)
	remove_obstacle()
	$HUD/MarginContainer/ScoreLabel.set_visible(false)
	# Removes bar interactivity
	remove_child($GhostBar)
	
	var over_menu = over_menu_scene.instance()
	over_menu.set_score($HUD.score)
	call_deferred("add_child", over_menu)


func _on_TimerInitObstacle_timeout():
	new_obstacle()
	$TimerObstacle.start()
